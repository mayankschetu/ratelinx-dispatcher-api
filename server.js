var Hapi = require('hapi');
var Server = new Hapi.Server();

Server.connection({
    port: 7002,
    host: '172.16.11.13',
    routes: {
        cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with', 'authorization']
        }
    }
});

/* +_+_+_+_+_+_+_+_+_+_+ Plugins / Middlewares +_+_+_+_+_+_+_+_+_+_+ */
Server.register(
    [
        require('hapi-auth-basic'),
        require('hapi-auth-jwt2'),
        {
            register: require('good'),
            options: {
                reporters: {
                    myConsoleReporter:
                        [
                            { module: 'good-console' },
                            'stdout'
                        ]
                }
            }
        },//logging the api & response time
    ], function (err) {
        if (err) {
            Server.log(['error'], 'hapi-swagger load error: ' + err)
        }
        else {
            Server.register([
                require('inert'),
                require('vision'),
                {
                    register: require('hapi-swagger'),
                    'options': {
                        protocol: 'https',
                        apiVersion: "0.0.1",
                        auth: false //'simple'
                    }
                }], (err) => {
                    if (err)
                        Server.log(['error'], 'hapi-swagger load error: ' + err)
                    else
                        Server.log(['start'], 'hapi-swagger interface loaded')
                });

            Server.log(['start'], 'hapi-swagger interface loaded')
        }
    });
/* +_+_+_+_+_+_+_+_+_+_+ End +_+_+_+_+_+_+_+_+_+_+ */

/* +_+_+_+_+_+_+_+_+_+_+_+_+_+_+ AUTH STRATEGIES +_+_+_+_+_+_+_+_+_+_+_+_+ */

var Auth = require('./authentication');//authentication module
var Config = require('./config/config');//server configurations

//Server.auth.strategy('simple', 'basic', { validateFunc: Auth.BasicAuth });//strategy to validate swagger access

Server.auth.strategy('dispatcherJWT', 'jwt',
    {
        key: Config.Secret,
        validateFunc: Auth.ValidateDispatcherJWT,
        verifyOptions: { algorithms: ['HS256'] }
    });//strategy to validate oauth JWT tokens

Server.auth.strategy('oauthJWT', 'jwt',
    {
        key: Config.Secret,
        validateFunc: Auth.ValidateOAuthJWT,
        verifyOptions: { algorithms: ['HS256'] }
    });//strategy to validate dispatcher JWT tokens

/* +_+_+_+_+_+_+_+_+_+_+_+_+_+_+ START SERVER +_+_+_+_+_+_+_+_+_+_+_+_+_+_+ */
console.log('|| ..... Starting the API server..... ||');
Server.start(() => {
    console.log('|| ..... Server running at:', Server.info.uri, '..... ||');
    require('./databases/UtilityFunc');//establish database connection
});

/* +_+_+_+_+_+_+_+_+_+_+_+_+_+_+ API ROUTES +_+_+_+_+_+_+_+_+_+_+_+_+_+_+ */

Server.route(require('./models'));//all the routes