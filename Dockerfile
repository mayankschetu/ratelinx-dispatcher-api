FROM node:6.10.0

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

EXPOSE 7002

CMD ["node", "server.js"]