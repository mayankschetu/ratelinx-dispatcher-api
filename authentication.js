var JWT = require('jsonwebtoken');
var Config = require('./config/config');

var MONGO = require('./databases/UtilityFunc');//mongo utility's instance

var redis = require('redis');//require the redis module
var client = redis.createClient(Config.redisConf); //redis client's instance for caching
client.auth(Config.REDIS_USERNAME);//authenticate the redis client

var AUTH = module.exports = {};

/**
 * Method to validate the OAuth2 JWT
 * @param {*} decoded - decoded token
 * @param {*} cb - callback
 */
AUTH.ValidateOAuthJWT = (decoded, req, cb) => {
    let isValid = (decoded.sub === 'oauth2') ? true : false;

    return cb(null, isValid);
};

/**
 * Method to validate the dispatcher' JWT
 * @param {*} decoded - decoded token
 * @param {*} cb - callback
 */
AUTH.ValidateDispatcherJWT = (decoded, req, cb) => {
    let isValid = (decoded.sub === 'dispatcher') ? true : false;

    return cb(null, isValid);
};

