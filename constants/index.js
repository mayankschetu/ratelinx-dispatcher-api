/**
 * Author: Arun Kumar
 * Created On: JUN 10, 2018
 * Created For: Created page constant data . 
 */

function define(name, value) {
    Object.defineProperty(exports, name, {
        value: value,
        enumerable: true
    });
}

var status = {
    NEW: {
        CODE: 'NEW',
        TEXT: 'New'
    },
    SENT: {
        CODE: 'SENT',
        TEXT: 'Shipment sent to driver'
    },
    ASSIGNED: {
        CODE: 'ASSIGNED',
        TEXT: 'Assigned'
    },
    ONTHEWAY: {
        CODE: 'ONTHEWAY',
        TEXT: 'Driver on the way'
    },
    ARRIVED:  {
        CODE: 'ARRIVED',
        TEXT: 'Driver arrived'
    },
    STARTED:  {
        CODE: 'STARTED',
        TEXT: 'Shipment started'
    },
    REACHED:  {
        CODE: 'REACHED',
        TEXT: 'Driver reached drop locations'
    },    
    UNLOADED:  {
        CODE: 'UNLOADED',
        TEXT: 'Vehicle unloaded'
    },
    COMPLETED:  {
        CODE: 'COMPLETED',
        TEXT: 'Completed'
    },
    EXPIRED:  {
        CODE: 'EXPIRED',
        TEXT: 'Expired'
    }
}

define('SHIPMENT_STATUS', status);





