#!/bin/bash

echo 'running deployment script..'

sshpass -p ${pwd} ssh -o StrictHostKeyChecking=no ${uname}@${ip} << EOF

cd ${path}

git checkout --force dayrunner-dev-wallet

git reset --hard HEAD

git pull

echo 'pulled new changes'

echo 'installing dependencies..'

npm install

echo 'dependencies installed..'

echo 'stop the server process..'

sudo forever stop server.js

echo 'server process stopped'

echo 'starting the new server process'

sudo forever start server.js

echo 'server process started'

echo 'successfully deployed'

EOF