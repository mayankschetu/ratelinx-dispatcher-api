#Truckr Node-Hapi API source code

#Installation
npm install

#Start the server
node server.js

#Run server on forever mode
forever start server.js

#Create indexes on following mongodb collections

##ShipmentDetails
mas_id
order_id
status
completedTimeStamp

##mastersPresenceDaily
mid
timestamp