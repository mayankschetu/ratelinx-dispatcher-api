'use strict'

var config = require('../config/config.json');
module.exports.status = function (successCode, data, language) {
    var return_;
    switch (successCode) {
        case 10:
            return_ = {
                "errNum": 10,
                "errFlag": 1,
                "errMsg": 'Your profile is currently being reviewed. please contact support for further queries.'
            }
            break;
        case 92:
            return_ = {
                "errNum": 92,
                "errFlag": 1,
                "errMsg": 'Your company got rejected or suspended by our admin, please contact your company for further details.'
            }
            break;
        case 79:
            return_ = {
                "errNum": 79,
                "errFlag": 1,
                "errMsg": 'Your profile got rejected by our admin, please contact our support for further queries.'
            }
            break;        
        case 38:
            return_ = { statusNum: 38, statusMsg: 'Vehicle Type Unavailable..', errFlag: 1, data: data };
            break;        
        case 78:
            return_ = { statusNum: 78, statusMsg: 'Thanks for the booking! We will get the best possible rider and send you the booking details shortly !', errFlag: 0, data: data };
            break;
        case 400:
            return_ = { statusNum: 400, statusMsg: 'Please Provide a file', errFlag: 0, data: data };
            break;
        case 500:
            return_ = { statusNum: 500, statusMsg: 'Error While Uploading File', errFlag: 0, data: data };
            break;
        case 2:
            return_ = { statusNum: 200, statusMsg: SuccessMessage(data), errFlag: 0, data: data };
            break;
        case 3:
            return_ = { statusNum: 500, statusMsg: "Error While Processing.", errFlag: 1, data: data };
            break;
        case 4:
            return_ = { statusNum: 409, statusMsg: data == 1 ? `This Email Address is already registered with another ${config.appName} account.` : `This Phone Number is already registered with another ${config.appName} account.`, errFlag: 1, data: data };
            break;        
        case 5:
            return_ = { statusNum: 201, statusMsg: 'User Created Successfully', errFlag: 0, data: data };
            break;
        case 6:
            return_ = { statusNum: 401, statusMsg: 'Unauthorized User', errFlag: 1, data: data };
            break;
        case 7:
            return_ = { statusNum: 7, statusMsg: 'Invalid token, please login or register.', errFlag: 1, data: data };
            break;
        case 8:
            return_ = { statusNum: 404, statusMsg: 'Driver not found', errFlag: 1, data: data };
            break;
        case 9:
            return_ = { statusNum: 404, statusMsg: 'Booking Not Found', errFlag: 1, data: data };
            break;
        case 10:
            return_ = { statusNum: 410, statusMsg: 'Shipment Expired', errFlag: 1, data: data };
            break;
        case 11:
            return_ = { statusNum: 200, statusMsg: 'Currently all Drivers are busy', errFlag: 0 };
            break;
        case 12:
            return_ = { statusNum: 301, statusMsg: 'Booking No Longer Available', errFlag: 1 };
            break;
        case 13:
            // return_ = {errNum: 200, errMsg: 'OTP has been sent to your registered Mobile Number.', errFlag: 0};
            return_ = { statusNum: 200, statusMsg: 'Verification Code has been sent to your registered Mobile Number', errFlag: 0 };
            break;
        case 14:
            return_ = { statusNum: 202, statusMsg: 'Driver Accepted', errFlag: 0 };
            break;
        case 15:
            return_ = { statusNum: 203, statusMsg: 'Driver Rejected', errFlag: 0 };
            break;
        case 16:
            return_ = { statusNum: 203, statusMsg: data + ' misssing', errFlag: 0 };
            break;
        case 17:
            return_ = { statusNum: 203, statusMsg: 'Please check Input Fields', errFlag: 1 };
            break;
        case 18:
            return_ = { statusNum: 200, statusMsg: 'Password Updated Successfully.', errFlag: 0 };
            break;
        case 19:
            return_ = { statusNum: 200, statusMsg: data == 1 ? "We Were Unable To Find Mobile Number" : "We Were Unable To Find Email", errFlag: 1, 'data': data };
            break;
        case 20:
            return_ = { statusNum: 200, statusMsg: 'Email has been sent. please verify.', errFlag: 0, 'data': data };
            break;
        case 21:
            return_ = { statusNum: 200, statusMsg: 'Added Successfully.', errFlag: 0, 'data': data };
            break;
        case 22:
            return_ = { statusNum: 200, statusMsg: 'Updated Successfully.', errFlag: 0, 'data': data };
            break;
        case 23:
            return_ = { statusNum: 200, statusMsg: 'Deleted Successfully.', errFlag: 0, 'data': data };
            break;
        case 24:
            return_ = { statusNum: 200, statusMsg: 'Successfully Logged Out.', errFlag: 0, 'data': data };
            break;
        case 25:
            return_ = { statusNum: 404, statusMsg: 'Shipment Not Found', errFlag: 1, 'data': data };
            break;
        case 26:
            return_ = { statusNum: 301, statusMsg: 'Shipment is already completed.', errFlag: 1, 'data': data };
            break;
        case 27:
            return_ = { statusNum: 301, statusMsg: 'Shipment status Updated already.', errFlag: 0, 'data': data };
            break;
        case 28:
            return_ = { statusNum: 301, statusMsg: 'Shipment Expired or Accepted By another', errFlag: 1 };
            break;
        case 29:
            return_ = { statusNum: 201, statusMsg: 'Rating already Given', errFlag: 0 };
            break;
        case 30:
            return_ = { statusNum: 201, statusMsg: 'Error while adding card please check Card details.', errFlag: 1, data: data };
            break;
        case 31:
            return_ = { statusNum: 200, statusMsg: 'Mobile Number is not exists, Please try after some time.', errFlag: 1, data: data };
            break;
        case 32:
            return_ = { statusNum: 404, statusMsg: 'Your vehicle is under review. Please contact to our support team.', errFlag: 1, data: data };
            break;
        case 33:
            return_ = { statusNum: 401, statusMsg: 'Phone Number or Email is Wrong.', errFlag: 1, data: data };
            break;
        case 34:
            return_ = { statusNum: 401, statusMsg: 'Password Is incorrect.', errFlag: 1, data: data };
            break;
        case 35:
            return_ = { statusNum: 401, statusMsg: 'Too Many Attempt Please Try After 6 hours.', errFlag: 1, data: data };
            break;

        case 36:
            return_ = { statusNum: 401, statusMsg: 'Incorrect Verification Code entered. Please try again.', errFlag: 1, data: data };
            break;

        case 37:
            return_ = { statusNum: 200, statusMsg: 'Error While Retriving Payment.', errFlag: 1, data: data };
            break;

        case 39:
            return_ = { statusNum: 200, statusMsg: 'Passanger has cancelled Booking.', errFlag: 1, data: data };
            break;

        case 40:
            return_ = { statusNum: 200, statusMsg: 'Account Added SuccessFully.', errFlag: 0, data: data };
            break;

        case 41:
            return_ = { statusNum: 400, statusMsg: 'Error While Adding Banck Account ', errFlag: 1, data: data };
            break;
        case 42:
            return_ = { statusNum: 400, statusMsg: 'No Bank Details Provided.', errFlag: 1, data: data };
            break;
        case 43:
            return_ = { statusNum: 200, statusMsg: 'Default Bank Account is Opted.', errFlag: 1, data: data };
            break;

        case 44:
            return_ = { statusNum: 200, statusMsg: 'Bank Details Removed SuccessFully.', errFlag: 1, data: data };
            break;

        case 45:
            return_ = { statusNum: 45, statusMsg: 'You have reached the Soft Limit.', errFlag: 1, data: data };
            break;
        case 46:
            return_ = { statusNum: 46, statusMsg: 'You have reached the Hard Limit.', errFlag: 1, data: data };
            break;

        default:
            return_ = { statusNum: 404, statusMsg: 'no data', errFlag: 1, 'data': data };

    }
    return return_;

}

module.exports.bookingStatus = function (successCode, language) {
    var return_;
    switch (successCode) {
        case 1:
            return_ = 'Booking requested';
            break;
        case 2:
            return_ = 'Driver accepted.';
            break;
        case 3:
            return_ = 'You cancelled.';
            break;
        case 4:
            return_ = 'Driver cancelled.';
            break;

        case 6:
            return_ = 'Driver is on the way.';
            break;
        case 7:
            return_ = 'Driver arrived.';
            break;
        case 8:
            return_ = 'Booking started.';
            break;
        case 9:
            return_ = 'Driver has reached to Drop Location.';
            break;
        case 10:
            return_ = 'Completed.';
            break;
        case 11:
            return_ = 'Booking expired.';
            break;
        case 12:
            return_ = {
                title: "You got New Booking",
                body: "Customer Has requested shipment",
                sound: "default"
            };
            break;
        case 13:
            return_ = 'Passanger has cancelled Booking.';
            break;
        case 16:
            return_ = 'Driver has unloded the load.';
            break;


        default:
            return_ = 'Unassigned.';

    }
    return return_;

}


module.exports.bookingStatus_notifyi = function (successCode, driver, language) {
    var return_;
    switch (successCode) {
        case 1:
            return_ = 'Thank you! We will inform you as soon as we find the best possible driver for your load.';
            break;
        case 2:
            return_ = 'Driver ' + driver.name + ' has accepted your load.';
            break;
        case 3:
            return_ = 'Driver has cancelled this delivery with the reason ' + driver.reason + '.';
            break;
        case 4:
            return_ = 'Your booking is cancelled, hope you book soon on ' + config.appName + ' again.';
            break;

        case 6:
            return_ = 'Driver ' + driver.name + ' is on his way to pickup your load , please be ready.';
            break;
        case 7:
            return_ = 'Driver ' + driver.name + ' has arrived at your address , please hand over the load to him';
            break;
        case 8:
            return_ = 'Vehicle has been loaded and our driver is on his way to drop your load.';
            break;
        case 9:
            return_ = 'Driver has Reached to Drop Location.';
            break;

        case 16:
            return_ = 'The load has been delivered.';
            break;

        case 10:
            return_ = 'Please check the receipt.';
            break;

        case 11:
            return_ = 'Sorry , we cannot take up your delivery request at this moment , all our drivers are busy. Please try again after sometime.';
            break;

        case 12:
            return_ = {
                title: "You got New Booking",
                body: "Customer Has requested shipment",
                sound: "default"
            };
            break;

        case 13:
            return_ = 'Customer has cancelled Booking.';
            break;


        case 14:
            return_ = 'You Got New Booking.';
            break;

        case 15:
            return_ = 'Verification code from ' + config.appName;
            break;


        default:
            return_ = 'Unassigned.';

    }
    return return_;

}

function SuccessMessage(successCode) {
    var message;
    switch (successCode) {
        case 1:
            message = 'OTP verified';
            break;
        case 2:
            message= 'Shipment sent to driver successully';
            break;
        default:
            message = 'Got The Details';
    }
    return message;
}

