var status = require('../statusMsg/status');
var config = require('../config/config.json');
var pubnub = require('./pubnub');

var FCM = require('fcm-push');

//param :  request.usertype, request.device_type, request.message request.push_token
module.exports.notifyFcm = function (request) {

    if (parseInt(request.usertype) == 1) {
        if (request.device_type == 1) {
            fcm_server_key = config.FCM_SERVER_KEY_MASTER_IOS;
        } else {
            fcm_server_key = config.FCM_SERVER_KEY_MASTER_ANDROID;
        }
    } else {
        if (request.device_type == 1) {
            fcm_server_key = config.FCM_SERVER_KEY_SLAVE_IOS;
        } else {
            fcm_server_key = config.FCM_SERVER_KEY_SLAVE_ANDROID;
        }
    }

    var fcm = new FCM(fcm_server_key);

    var message = {
        to: request.push_token, // required fill with device token or topics
        collapse_key: 'your_collapse_key',
        priority: 'high',
        "delay_while_idle": true,
        "dry_run": false,
        "time_to_live": 3600,
        "content_available": true,
        mutable_content: true,
        badge: "1",
        data: request.message
    };

    //    if (request.device_type == 1)
    message.notification = {
        title: request.notification,
        body: request.notification,
        sound: "default"
    };

    return;
}

module.exports.notifyFcmTopic = function (request, callback) {

    if (request.push_token === '') return 0;//return if the token is empty

    console.log(request);
    let fcm_server_key = '';

    if (parseInt(request.usertype) == 1) {
        if (request.device_type == 1) {
            fcm_server_key = config.FCM_SERVER_KEY_MASTER_IOS;
        } else {
            fcm_server_key = config.FCM_SERVER_KEY_MASTER_ANDROID;
        }
    } else {
        if (request.device_type == 1) {
            fcm_server_key = config.FCM_SERVER_KEY_SLAVE_IOS;
        } else {
            fcm_server_key = config.FCM_SERVER_KEY_SLAVE_ANDROID;
        }
    }

    console.log(fcm_server_key);


    var fcm = new FCM(fcm_server_key);

    var message = {
        to: '/topics/' + request.push_token, // required fill with device token or topics
        // collapse_key: 'your_collapse_key',
        priority: 'high',
        // "delay_while_idle": true,
        // "dry_run": false,
        // "time_to_live": 3600,
        "content_available": true,
        mutable_content: true,
        // badge: "1",
        data: request.message
        // data: { title: 'Test Topic Push', body: 'Test Topic Push', sound: 'default' }
    };

    if (parseInt(request.device_type) == 2)
        message.notification = {
            title: request.notification,
            body: request.notification,
            sound: "default"
        };
    fcm.send(message)
        .then(function (response) {
            console.log(message);
            console.log("Successfully sent with response: ", response);
            // return callback('done');
            return 1;
        })
        .catch(function (err) {
            console.log("Something has gone wrong!");
            console.error(err);

            // return callback('done');
            return 0;
        });

    return;
}

module.exports.notifyFcmTopicMaster = (request, callback) => {

    if (request.push_token === '') return 0;//return if the token is empty

    console.log(request);
    let fcm_server_key = '';

    if (parseInt(request.usertype) == 1) {
        if (request.device_type == 1) {
            fcm_server_key = config.FCM_SERVER_KEY_MASTER_IOS;
        } else {
            fcm_server_key = config.FCM_SERVER_KEY_MASTER_ANDROID;
        }
    } else {
        if (request.device_type == 1) {
            fcm_server_key = config.FCM_SERVER_KEY_SLAVE_IOS;
        } else {
            fcm_server_key = config.FCM_SERVER_KEY_SLAVE_ANDROID;
        }
    }

    console.log(fcm_server_key);


    var fcm = new FCM(fcm_server_key);

    var message = {
        to: '/topics/' + request.push_token, // required fill with device token or topics
        "mutable_content": true,
        "data": {
            a: 506,
            b: 'hello'
        },
        "notification":
            {
                "body": " ",
                "title": "Location Updating",
                "sound": "default",
                // "a": "506"
            }

    };
   
    fcm.send(message)
        .then(function (response) {
            console.log(message);
            console.log("Successfully sent with response: ", response);
            // return callback('done');
            return 1;
        })
        .catch(function (err) {
            console.log("Something has gone wrong!");
            console.error(err);

            // return callback('done');
            return 0;
        });

    return;
}

module.exports.notifyPunbun = function (request) {
    if (Array.isArray(request.listner)) {
        // we can not publish to multiple channle this feture is not available 
        request.listner.forEach(channel => {
            pubnub.publish(channel, request.message, function (resp) {
                console.log('array pubnublished' + resp);
                return resp;            
            });
        });
    } else {
        pubnub.publish(request.listner, request.message, function (resp) {
            return resp;            
        });
    }
}

module.exports.notifyFcmTest = function (request, cb) {
    fcm_server_key = request.payload.serverKey;
    var fcm = new FCM(fcm_server_key);
    var message = {
        to: request.payload.pushToken, // required fill with device token or topics
        collapse_key: 'your_collapse_key',
        priority: 'high',
        "delay_while_idle": true,
        "dry_run": false,
        "time_to_live": 3600,
        "content_available": true,
        badge: "1",

    };

    console.log(message);
    console.log(request.payload);
    fcm.send(message)
        .then(function (response) {
            console.log("push sent");
            console.log(response);
            cb(response);
        })
        .catch(function (err) {
            console.log("push not sent");
            console.log(err);
            cb(err)
        })
}