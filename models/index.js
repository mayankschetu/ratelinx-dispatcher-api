/***
 * Author: Arun Kumar
 * Created On: MAY 1, 2018
 * Created For: THIS IS FOR ALL ROUTING. 
 **/

var Joi = require('joi');
var dispatcher = require('./dispatcher');
var driver = require('./driver');
var oauth = require('./oauth');
var redisSubscriber = require('./redisSubscriber');

var headerAuthValidator = Joi.object({
    'authorization': Joi.string().required().description('header authorization')
}).unknown();

module.exports = [
    {
        method: 'post',
        path: '/api/dispatcher/signIn',
        config: {
            tags: ['api', 'dispatcher', 'sign in'],
            description: 'dispatcher sign in',
            notes: "Dispatcher sign in at dispatcher App using API at RateLinx",
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
                responses: {
                    '200': {
                        'description': 'Success',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    },
                    '400': {
                        'description': 'Bad Request',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    }
                }
            },
            validate: {
                payload:
                {
                    clientID: Joi.string().required().required().description('Dispatcher client id'),
                    userName: Joi.string().required().description('Dispatcher username'),
                    password: Joi.string().required().description('Dispatcher password'),
                    fromApp: Joi.string().required().description('Application type eg- Mobile'),
                    fromDevice: Joi.string().required().description('Dispatcher device eg- Android 5.1.2'),
                    apps: Joi.array().required().description('eg- ["Shipping"]'),
                    ip: Joi.string().description('Dispatcher ip address eg- 172.16.11.13')
                }
            }
        },
        handler: dispatcher.signIn
    },
    {
        method: 'get',
        path: '/api/dispatcher/signOut',
        config: {
            tags: ['api', 'dispatcher', 'sign out'],
            description: 'dispatcher sign out',
            notes: "Dispatcher sign out at dispatcher App using API at RateLinx",
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
                responses: {
                    '200': {
                        'description': 'Success',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    },
                    '400': {
                        'description': 'Bad Request',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    }
                }
            },
            validate: {
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.signOut
    },
    {
        method: 'get',
        path: '/api/dispatcher/getAppConfig',
        config: {
            tags: ['api', 'master'],
            description: 'get app configuration',
            notes: 'api returns, unique configuration which is handled by admin',
            auth: 'dispatcherJWT',
            validate: {
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.getAppConfig
    },
    {
        method: 'get',
        path: '/api/dispatcher/shipmentsFrRL',
        config: {
            tags: ['api', 'dispatcher', 'rateLinx future pickups'],
            description: 'Get the shipments of a logged in dispatcher',
            notes: 'Get the shipments of a logged in dispatcher',
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.shipmentsFrRL
    },
    {
        method: 'get',
        path: '/api/dispatcher/shipmentDetailsFrRL/{clientID}/{bolNum}',
        config: {
            tags: ['api', 'dispatcher', 'shipment details from ratelinx'],
            description: 'Get the details of a shipment of a logged in dispatcher',
            notes: 'Get the details of a shipments of a logged in dispatcher',
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerAuthValidator,
                params:
                {
                    clientID: Joi.string().required().description('client ID of shipment from RateLinx'),
                    bolNum: Joi.string().required().description('bolNum of shipment from RateLinx'),
                }
            }
        },
        handler: dispatcher.shipmentDetailsFrRL
    },
    {
        method: 'get',
        path: '/api/dispatcher/shipmentDetails/{clientID}/{bolNum}',
        config: {
            tags: ['api', 'dispatcher', 'shipment details from ratelinx'],
            description: 'Get the details of a shipment of a logged in dispatcher',
            notes: 'Get the details of a shipments of a logged in dispatcher',
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerAuthValidator,
                params:
                {
                    clientID: Joi.string().required().description('client Id of shipment from RateLinx'),
                    bolNum: Joi.string().required().description('bolNum of shipment from RateLinx'),
                }
            }
        },
        handler: dispatcher.shipmentDetails
    },
    {
        method: 'get',
        path: '/api/dispatcher/driversFrRL',
        config: {
            tags: ['api', 'dispatcher'],
            description: 'Get the drivers of a logged in dispatcher',
            notes: 'Get the drivers of a logged in dispatcher',
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.driversFrRL
    },
    {
        method: 'POST',
        path: '/api/dispatcher/assignShipmentToDriver',
        config: {
            tags: ['api', 'app'],
            description: 'Assign a shipment to a driver',
            notes: 'Insert if not exist or update dispatched object with driver details in the collection-shipment_details and push shipment details in current_assignments array in collection-driver details ',
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
                responses: {
                    '200': {
                        'description': 'Success',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    },
                    '400': {
                        'description': 'Bad Request',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    }
                }
            },
            validate: {
                payload: {
                    clientID: Joi.string().required().description('Client ID of an future pickup'),
                    bolNum: Joi.string().required().description('BolNum of an future pickup'),
                    driverID: Joi.string().required().description('Id of driver at RateLinx')
                },
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.assignShipmentToDriver
    },
    {
        method: 'post',
        path: '/api/dispatcher/allShipments',
        config: {
            tags: ['api', 'dispatcher'],
            description: 'Get all the shipments of a logged in dispatcher',
            notes: 'Get all the shipments of a logged in dispatcher',
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                payload:
                {
                    startDate: Joi.string().trim().description('start date, format: YYYY-MM-DD'),
                    endDate: Joi.string().trim().description('end date, format:- YYYY-MM-DD'),
                    pageIndex: Joi.number().integer().description('page index, default: 0')
                },
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.allShipments
    },
    {
        method: 'get',
        path: '/api/dispatcher/unAssignedShipments',
        config: {
            tags: ['api', 'dispatcher'],
            description: 'Get un-Assigned shipments of a logged in dispatcher',
            notes: 'Get un-Assigned shipments of a logged in dispatcher',
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.unAssignedShipments
    },
    {
        method: 'get',
        path: '/api/dispatcher/allDrivers',
        config: {
            tags: ['api', 'dispatcher'],
            description: 'Get all the drivers of a logged in dispatcher',
            notes: 'Get all the drivers of a logged in dispatcher',
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.allDrivers
    },
    {
        method: 'get',
        path: '/api/dispatcher/driverDetails/{driverID}',
        config: {
            tags: ['api', 'dispatcher'],
            description: 'Get the details of a driver under a logged in dispatcher',
            notes: 'Get the details of a driver under a logged in dispatcher',
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerAuthValidator,
                params:
                {
                    driverID: Joi.string().required().description('driver Id at RateLinx')
                }
            }
        },
        handler: dispatcher.driverDetails
    },
    {
        method: 'post',
        path: '/api/dispatcher/confirmOrDenyShipmentFrRL',
        config: {
            tags: ['api', 'dispatcher', 'confirm shipment'],
            description: 'To confirm an awarded shipment using API at RateLinx',
            notes: "To confirm an awarded shipment using API at RateLinx",
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
                responses: {
                    '200': {
                        'description': 'Success',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    },
                    '400': {
                        'description': 'Bad Request',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    }
                }
            },
            validate: {
                payload:
                {
                    clientID: Joi.string().trim().required().description('Client id of shipment'),
                    bolNum: Joi.string().trim().required().description('BolNum of shipment'),
                    action: Joi.number().integer().min(1).max(2).required().description('1 for confirm and 2 for deny'),
                    proNumber: Joi.when('action', { is: 1, then: Joi.string().trim().min(3).required() }).description('PRO number if action is 1 ie confirm'),
                    denyReason: Joi.when('action', { is: 2, then: Joi.string().trim().min(10).required() }).description('Deny reason if action is 2 ie deny'),
                },
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.confirmOrDenyShipmentFrRL
    },
    {
        method: 'get',
        path: '/api/driver/assignedShipments/{driverID}',
        config: {
            tags: ['api', 'driver'],
            description: 'get shipments assigned to driver',
            notes: 'get driver id and return shipments of the driver',
            auth: 'oauthJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
                responses: {
                    '200': {
                        'description': 'Success',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    },
                    '400': {
                        'description': 'Bad Request',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    }
                }
            },
            validate: {
                params: {
                    driverID: Joi.string().required().description('driver ID'),
                }
            }
        },
        handler: driver.assignedShipments
    },
    {
        method: 'POST',
        path: '/api/dispatcher/insertRLShipments',
        config: {
            tags: ['api', 'dispatcher'],
            description: 'insert ratelinx shipments into disptcher DB',
            notes: 'get shipments from ratelinx and insert new shipments into dispatcher DB',
            auth: 'dispatcherJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
                responses: {
                    '200': {
                        'description': 'Success',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    },
                    '400': {
                        'description': 'Bad Request',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    }
                }
            },
            validate: {
                headers: headerAuthValidator,
            }
        },
        handler: dispatcher.insertRLShipments
    },
    {
        method: 'PUT',
        path: '/api/driver/shipmentStatus',
        config: {
            tags: ['api', 'driver'],
            description: 'shipment status update by driver',
            notes: 'get new status and update',
            auth: 'oauthJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
                responses: {
                    '200': {
                        'description': 'Success',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    },
                    '400': {
                        'description': 'Bad Request',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    }
                }
            },
            validate: {
                payload: {
                    driverID: Joi.string().required().description('driver ID'),
                    status: Joi.string().required().description('6- OntheWay , 7- Arrived, 8- Loaded And Delivery started, 9- reached at location ,16-unloaded , 10- completed'),
                    bolNum: Joi.string().required().description('bolNum of shipment from RateLinx'),
                    clientID: Joi.string().required().description('client ID of shipment from RateLinx'),
                    lat: Joi.any().description('latitude'),
                    long: Joi.any().description('longitude'),
                },
                headers: headerAuthValidator
            }
        },
        handler: driver.shipmentStatus
    },
    {
        method: 'POST',
        path: '/api/dispatcher/assignShipmentToDriverMobile',
        config: {
            tags: ['api', 'app'],
            description: 'Assign a shipment to a driver',
            notes: 'Insert if not exist or update dispatched object with driver details in the collection-shipment_details and push shipment details in current_assignments array in collection-driver details ',
            auth: 'oauthJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
                responses: {
                    '200': {
                        'description': 'Success',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    },
                    '400': {
                        'description': 'Bad Request',
                        'schema': Joi.object({
                            equals: Joi.number(),
                        }).label('Result')
                    }
                }
            },
            validate: {
                payload: {
                    clientID: Joi.string().required().description('Client ID of future pickup'),
                    bolNum: Joi.string().required().description('BolNum of future pickup'),
                    driverID: Joi.string().required().description('User ID of driver at RateLinx'),
                    dispatcherID: Joi.string().required().description('ClientId of dispatcher at RateLinx'),
                    dispatcherToken: Joi.string().required().description('Session Token of dispatcher at RateLinx')
                },
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.assignShipmentToDriverMobile
    },
    {
        method: 'get',
        path: '/api/dispatcher/allDriversMobile/{dispatcherID}',
        config: {
            tags: ['api', 'dispatcher'],
            description: 'Get all the drivers of a logged in dispatcher',
            notes: 'Get all the drivers of a logged in dispatcher',
            auth: 'oauthJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                params: {
                    dispatcherID: Joi.string().required().description('ClientId of dispatcher at RateLinx')
                },
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.allDriversMobile
    },
    {
        method: 'get',
        path: '/api/shipments/activityPath/{clientID}/{bolNum}',
        config: {
            tags: ['api', 'dispatcher'],
            description: 'Get the actual path of the shipment.',
            notes: 'Get the actual path of the shipment.',
            auth: 'oauthJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
            },
            validate: {
                params:
                {
                    clientID: Joi.string().required().description('Shipment clientID'),
                    bolNum: Joi.string().required().description('Shipment bolNum'),
                },
                headers: headerAuthValidator
            }
        },
        handler: dispatcher.activityPath
    },
    {
        method: 'get',
        path: '/oauth/{clientID}/{redirectURL}',
        config: {
            tags: ['api', 'dispatcher'],
            description: 'Get the authorization code.',
            notes: 'Get the client id, redirect url and return authorization code.',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
            },
            validate: {
                params:
                {
                    clientID: Joi.string().required().description('Client ID of client'),
                    redirectURL: Joi.string().required().description('Redirect URL of client'),
                },
                headers: headerAuthValidator
            }
        },
        handler: oauth.authGrant
    },
    {
        method: 'post',
        path: '/oauth/accessToken',
        config: {
            tags: ['api', 'dispatcher'],
            description: 'Get the access token',
            notes: 'Get the client secret, authorization code and return the access token',
            auth: false,
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                },
            },
            validate: {
                payload:
                {
                    clientID: Joi.string().required().description('Client ID of client'),
                    clientSecret: Joi.string().required().description('Client secret of client'),
                    authCode: Joi.when('grantType', { is: 1, then: Joi.string().required() }).description('Authorization code'),
                    grantType: Joi.number().integer().min(1).max(2).required().description('Grant type, eg: authorization code- 1, client credentials- 2'),
                }
            }
        },
        handler: oauth.accessToken
    },
]