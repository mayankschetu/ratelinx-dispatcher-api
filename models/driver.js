/**
 * Author: Arun Kumar
 * Created On: MAY 25, 2018
 * Created For: This is for handling the driver related actions. 
 **/
var Mongo = require('../databases/UtilityFunc');
var async = require("async");
var status = require('../statusMsg/status');
var moment = require('moment');
var config = require('../config/config.json');
var constants = require('../constants/index');
var notifyi = require("./NotifyUser");
var redis = require('redis');
var client = redis.createClient(config.redisConf);
client.auth(config.REDIS_USERNAME);

/**
 * Author: Arun Kumar
 * Func Name: assignedShipments
 * Created On: May 25, 2018
 * Created For: To get the assigned shipments of a driver from dispatcher DB
 * @Param request:  Object with url parameters
 *              *request.params.driverID for driver ID
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.assignedShipments = function (request, reply) {
    var condition = {};

    condition = { 
        "AssignedDriverID": request.params.driverID, 
        "Status": { "$nin": [
            constants.SHIPMENT_STATUS.COMPLETED.CODE,
            constants.SHIPMENT_STATUS.EXPIRED.CODE,
            constants.SHIPMENT_STATUS.NEW.CODE
    ] } };

    async.waterfall(
        [
            function (callback) {
                Mongo.SelectWIthLimitAndIndex("shipment_details", condition, 0, function (err, shipments) {
                    if (err)
                        return callback(status.status(3));
                    var assigned = [];
                    shipments.forEach(shipment=>{
                      var  obj = {
                          BolNum: shipment.BolNum,
                          ClientID: shipment.ClientID,
                          LocID: shipment.LocID,
                          AssignedAtTS: 0
                      };
                        shipment.Dispatched.forEach(dispatched => {
                            if(dispatched.DriverID === request.params.driverID) {
                                obj.AssignedAtTS = dispatched.ReceivedAtTS;
                            }
                        });
                    assigned.push(obj);

                    });

                    return callback(null, assigned);
                });
            },
            function (assigned, callback) {
                Mongo.SelectOne("driver_details", { DriverID: request.params.driverID }, function (err, driver) {
                    if (err)
                        return callback(status.status(3));

                    return callback(null, assigned, driver);
                });
            }
        ],
        function (err, assigned, driver) {
            if (err)
                return reply(err);

            reply(status.status(2, { shipments: assigned }));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: LogBookingActivity
 * Created On: May 28, 2018
 * Created For: To add the driver activity for booking
 * @Param currentBookings: array of current bookings of driver 
 * @Param params: Object with properties- BolNum, Status, Msg, Time, Lat and Long 
 * @Param callback: callback function to call
 * @return: return data in json format
 **/
function LogBookingActivity(currentBookings, params, callback) {

    async.forEach(currentBookings, (item, cb) => {

        let data = {
             
            $push: {
                Activities:
                    {
                        BolNum: params.BolNum,
                        ClientID: params.ClientID,
                        Status: params.Status,
                        Msg: params.Msg,
                        Time: params.Time,
                        Lat: params.Lat,
                        Long: params.Long
                    }
            }
        };

        let options = { upsert: true };

        let locationLogsData = {
            $set: {
                Lat: params.Lat,
                Long: params.Long,
                CurrentStatus: params.Status,
                CurrentStatusText: params.Msg
            },
            $push: {
                Activities:
                    {
                        BolNum: params.BolNum,
                        ClientID: params.ClientID,
                        Status: params.Status,
                        Msg: params.Msg,
                        Time: params.Time,
                        Lat: params.Lat,
                        Long: params.Long
                    }
            }
        };

        if (item.BolNum == params.BolNum && item.ClientID == params.ClientID){
            locationLogsData['$push'][params.Status] = params.Lat + ',' + params.Long;

        Mongo.FINDONEANDUPDATE(
            'location_logs',
            { query: { ClientID: params.ClientID, BolNum: params.BolNum }, data: locationLogsData, options: options },
            // () => { return callback(null, 'done') });
            () => {
                return;
            });

        Mongo.FINDONEANDUPDATE(
            'shipment_details',
            { query: { ClientID: item.ClientID, BolNum: item.BolNum }, data: data, options: options },
            // () => { return callback(null, 'done') });
            () => {
                return;
            });
        }

        return cb(null, 'done');

    }, (err, result) => {
        return callback(null, 'done')
    });
}

/**
 * Author: Arun Kumar
 * Func Name: shipmentStatus
 * Created On: May 28, 2018
 * Created For: To update shipment status in dispatcher DB
 * @Param request:  Object with url parameters
 *              *request.params.driverID for driver ID
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.shipmentStatus = function (request, reply) {
    var driverDetails = {};
    async.waterfall([
        function (callback) {
            Mongo.SelectOne(
                "driver_details",
                { 'DriverID': request.payload.driverID },
                function (err, driver) {
                    if (err)
                        return reply(status.status(3));

                    if (driver === null)
                        return reply(status.status(25));

                    driverDetails = driver;
                    return callback(null, driver);
                });
        }, //get the driver details and the plans

        function (driver, callback) {

            Mongo.SelectOne(
                "shipment_details",
                { 'ClientID': request.payload.clientID, 'BolNum': request.payload.bolNum, 'AssignedDriverID': request.payload.driverID },
                function (err, shipment) {
                    if (err)
                        return reply(status.status(3));

                    if (shipment === null)
                        return reply(status.status(25));

                    return callback(null,driver, shipment);
                });
        }//check if the shipment is valid & get all the shipment details

    ], (err, driver, shipment) => {
        
        if (err)
            return reply(err);

        let timestamp = moment().unix();//time of the booking status updates
        switch (shipment.Status) {
            //shipment already completed
            case constants.SHIPMENT_STATUS.COMPLETED.CODE:
                if (request.payload.status == constants.SHIPMENT_STATUS.COMPLETED.CODE)
                    return reply(status.status(26));

                break;

            default:
                var dataToUpdate = {};
                switch (request.payload.status) {
                    //arrived at pickup location DriverOnTheWayTime
                    case constants.SHIPMENT_STATUS.ONTHEWAY.CODE:                    
                        dataToUpdate = {
                            'Status': request.payload.status,
                            'StatusText': constants.SHIPMENT_STATUS.ONTHEWAY.TEXT, //'Driver on the way',
                            'ShipmentDetails.Status': request.payload.status,
                            'ShipmentDetails.DriverOnTheWayTime': timestamp
                        };

                        //log the booking activity
                        LogBookingActivity(
                            driverDetails.currentAssignments,
                            {
                                BolNum: shipment.BolNum,
                                ClientID: shipment.ClientID,
                                Status: request.payload.status,
                                Msg: dataToUpdate.StatusText,
                                Time: timestamp,
                                Lat: request.payload.lat || '',
                                Long: request.payload.long || ''
                            }, () => {
                            });
                        break;

                    //arrived at pickup location DriverOnTheWayTime
                    case constants.SHIPMENT_STATUS.ARRIVED.CODE:                    
                        dataToUpdate = {
                            'Status': request.payload.status,
                            'StatusText': constants.SHIPMENT_STATUS.ARRIVED.TEXT, //'Driver arrived',
                            'ShipmentDetails.Status': request.payload.status,
                            'ShipmentDetails.DriverArrivedTime': timestamp,
                        };

                        //log the booking activity
                        LogBookingActivity(
                            driverDetails.currentAssignments,
                            {
                                BolNum: shipment.BolNum,
                                ClientID: shipment.ClientID,
                                Status: request.payload.status,
                                Msg: dataToUpdate.StatusText,
                                Time: timestamp,
                                Lat: request.payload.lat || '',
                                Long: request.payload.long || ''
                            }, () => {
                            });
                        break;

                    //shipment started
                    case constants.SHIPMENT_STATUS.STARTED.CODE:
                        
                        dataToUpdate = {
                            'Status': request.payload.status,
                            'StatusText': constants.SHIPMENT_STATUS.STARTED.TEXT, //'Shipment started',
                            'ShipmentDetails.Status': request.payload.status,
                            'ShipmentDetails.DriverLoadedStartedTime': timestamp
                        };

                        //log the booking activity
                        LogBookingActivity(
                            driverDetails.currentAssignments,
                            {
                                BolNum: shipment.BolNum,
                                ClientID: shipment.ClientID,
                                Status: request.payload.status,
                                Msg: dataToUpdate.StatusText,
                                Time: timestamp,
                                Lat: request.payload.lat || '',
                                Long: request.payload.long || ''
                            }, () => {
                            });
                        break;

                    //reached drop location
                    case constants.SHIPMENT_STATUS.REACHED.CODE:

                        dataToUpdate = {
                            'Status': request.payload.status,
                            'StatusText': constants.SHIPMENT_STATUS.REACHED.TEXT, //'Driver reached drop location',
                            'ShipmentDetails.Status': request.payload.status,
                            'ShipmentDetails.DriverReachedDropLocationTime': timestamp,
                        };

                        //log the booking activity                        
                        LogBookingActivity(
                            driver.currentAssignments,
                            {
                                BolNum: shipment.BolNum,
                                ClientID: shipment.ClientID,
                                Status: request.payload.status,
                                Msg: dataToUpdate.StatusText,
                                Time: timestamp,
                                Lat: request.payload.lat || '',
                                Long: request.payload.long || ''
                            }, () => {
                            });
                        break;

                    //unloaded
                    case constants.SHIPMENT_STATUS.UNLOADED.CODE:                      

                        dataToUpdate = {
                            "Status": request.payload.status,
                            'StatusText': constants.SHIPMENT_STATUS.UNLOADED.TEXT, //'Vehicle unloaded',
                            "ShipmentDetails.Status": request.payload.status,
                            "ShipmentDetails.DriverDropedTime": timestamp, // unloaded timestamp                            
                        };   

                        //log the booking activity
                        LogBookingActivity(
                            driverDetails.currentAssignments,
                            {
                                BolNum: shipment.BolNum,
                                ClientID: shipment.ClientID,
                                Status: request.payload.status,
                                Msg: dataToUpdate.StatusText,
                                Time: timestamp,
                                Lat: request.payload.lat || '',
                                Long: request.payload.long || ''
                            }, () => {
                            });
                        break;

                    //complete the booking
                    case constants.SHIPMENT_STATUS.COMPLETED.CODE:

                        dataToUpdate = {
                            "Status": request.payload.status,
                            "StatusText": constants.SHIPMENT_STATUS.COMPLETED.TEXT, //"Completed",
                            "ShipmentDetails.Status": request.payload.status,
                            "ShipmentDetails.DriverCompletedTime": timestamp,                            
                            "CompletedAtTS": timestamp,
                        };                        

                        Mongo.FINDONEANDUPDATE(
                            'driver_details',
                            {
                                query: { DriverID: request.payload.driverID },
                                data: {
                                    //$inc: { currentAssignmentsCount: -1 },
                                    $pull: { currentAssignments: { BolNum: shipment.BolNum } }
                                }
                            }, (err, result) => {

                                LogBookingActivity(
                                    driverDetails.currentAssignments,
                                    {
                                        BolNum: shipment.BolNum,
                                        ClientID: shipment.ClientID,
                                        Status: request.payload.status,
                                        Msg: dataToUpdate.StatusText,
                                        Time: timestamp,
                                        Lat: request.payload.lat || '',
                                        Long: request.payload.long || ''
                                    }, () => {
                                    });
                            });

                        break;
                }                
               
                //================ dispatcher notification =========>
                let dispatcherData = {
                    status: request.payload.status,
                    bid: shipment.BolNum,
                    clientID: shipment.ClientID,
                    name: driverDetails.FullName,
                    email: driverDetails.Email,
                    time: timestamp,
                };
                let dispatcherChannel = 'bookingChn' + shipment.DispatcherID;
                notifyi.notifyPunbun({ 'listner': dispatcherChannel, message: dispatcherData });
                //====================================================>
                
                Mongo.Update(
                    "shipment_details",
                    { 'ClientID': request.payload.clientID, 'BolNum': request.payload.bolNum },
                    dataToUpdate,
                    function (err, bookings_) {
                        if (err)
                            return reply(status.status(3));

                        return reply(status.status(22));
                    });            
        }
    });
}