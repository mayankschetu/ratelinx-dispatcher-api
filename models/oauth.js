/**
 * Author: Arun Kumar
 * Created On: JUNE 15, 2018
 * Created For: This is for handling the oauth related actions. 
 **/

var async = require("async");
var ext_request = require('request');
var jwt = require('jsonwebtoken');
var moment = require('moment');
var config = require('../config/config.json');
var status = require('../statusMsg/status');
var Mongo = require('../databases/UtilityFunc');

/**
 * Author: Arun Kumar
 * Func Name: authGrant
 * Created On: Jun 15, 2018
 * Created For: To authenticate client and generate authorization code.
 * @Param req:  Object with request data
 *              *req.payload.clientID for client ID of client 
 *              *req.payload.redirectURL for redirect url of client
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.authGrant = function (request, reply) {
    async.waterfall(
        [
            function (callback) {
                Mongo.SelectOne("oauth_clients", { ClientID: request.params.clientID }, function (error, clientDetails) {
                    if (error) return reply(error);

                    return callback(null, clientDetails);
                });
            }
        ],
        function (error, clientDetails) {
            if (error) return reply(error);

            var authCode = jwt.sign(clientDetails, config.Secret, {
                expiresIn: config.OAuthTokenExpiresIn
            });

            reply.redirect(request.payload.redirectURL + "?code=" + authCode);
        });
}

/**
 * Author: Arun Kumar
 * Func Name: accessToken
 * Created On: Jun 18, 2018
 * Created For: To generate access token.
 * @Param req:  Object with request data
 *              *req.payload.clientID for client ID of client 
 *              *req.payload.clientSecret for client secret of client 
 *              *req.payload.grantType for type of grant 
 *              *req.payload.authCode for authorization code received in step one if grantType is 1
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.accessToken = function (request, reply) {
    async.waterfall(
        [
            function (callback) {
                if (request.payload.grantType == 1) {
                    jwt.verify(request.payload.authCode, config.Secret, function (error, decoded) {
                        if (error) return reply(error);

                        return callback(null, decoded);
                    });
                } else {
                    return callback(null, true);
                }
            },
            function (decoded, callback) {
                Mongo.SelectOne("oauth_clients", { ClientID: request.payload.clientID, ClientSecret: request.payload.clientSecret }, function (error, clientDetails) {
                    if (error) return reply(error);

                    return callback(null, clientDetails);
                });
            }
        ],
        function (error, clientDetails) {
            if (error) return reply(error);
            if (clientDetails == null) return reply({ statusNum: 400, statusMsg: 'Bad Request', errFlag: 1 });;
            var accessToken = jwt.sign(clientDetails, config.Secret, {
                expiresIn: config.OAuthTokenExpiresIn,
                subject: 'oauth2'
            });

            reply(status.status(2, { 'accessToken': accessToken, expiresIn: config.OAuthTokenExpiresIn }));
        });
}