var redis = require('redis');
var moment = require('moment');

var config = require('../config/config.json');
var Utility = require('../databases/UtilityFunc');
var notifyi = require("./NotifyUser");
var constants = require('../constants/index');

var client = redis.createClient(config.redisConf); //creates a new client
client.auth(config.REDIS_USERNAME);
ObjectID = require('mongodb').ObjectID;


// start redis subscriber
subscriber = redis.createClient(config.redisConf);
subscriber.auth(config.REDIS_USERNAME);
subscriber.psubscribe('__key*__:*')


subscriber.on("pmessage", function (pattern, channel, message) {
    switch (channel) {
        case '__keyevent@0__:hset':
            break;
        case '__keyevent@0__:del':
            break;
        case '__keyevent@0__:expired':
            console.log(message);
            var booking = message.split("_");
            switch (booking[0]) {
                case 'que':
                Utility.SelectOne('shipment_details', { ClientID: booking[1],  BolNum: booking[2] }, function (err, shipment) {
                    if (err) {
                        console.log(err);
                    } 
                    if (shipment) {
                        Utility.Update('shipment_details', { ClientID: booking[1], BolNum: booking[2] }, { 'InDispatch': false, 'Status': constants.SHIPMENT_STATUS.EXPIRED.CODE, 'StatusText': constants.SHIPMENT_STATUS.EXPIRED.TEXT, "ShipmentDetails.Status": constants.SHIPMENT_STATUS.EXPIRED.CODE }, function (err, result) {
                            if (err) {
                                console.log(err);
                            } else {
                                let dispatcherData = {
                                    status: constants.SHIPMENT_STATUS.EXPIRED.CODE,
                                    clientID: booking[1],
                                    bid: booking[2],
                                    name: '',
                                    email: '',
                                    time: moment.unix()
                                };
                                let dispatcherChannel = 'bookingChn' + shipment.DispatcherID;
                                console.log('dischannel'+ dispatcherChannel);
                                notifyi.notifyPunbun({ 'listner': dispatcherChannel, message: dispatcherData });
                            }
                        });
                    }
                });
                break;                
            }
            break;
    }
});

// end redis subscriber

module.exports = {
   
}