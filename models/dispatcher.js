/**
 * Author: Arun Kumar
 * Created On: MAY 1, 2018
 * Created For: This is for handling the dispatcher related actions. 
 **/

var async = require("async");
var ext_request = require('request');
var jwt = require('jsonwebtoken');
var moment = require('moment');
var config = require('../config/config.json');
var status = require('../statusMsg/status');
var Mongo = require('../databases/UtilityFunc');
var notifyi = require("./NotifyUser");
var redis = require('redis');
var constants = require('../constants/index');
var client = redis.createClient(config.redisConf);
client.auth(config.REDIS_USERNAME);

/**
 * Author: Arun Kumar
 * Func Name: signIn
 * Created On: May 01, 2018
 * Created For: To sign the dispatcher in the application on the basis of successful signin at RateLinx.
 * @Param req:  Object with request data
 *              *req.payload.ClientID for dispatcher ID at RateLinx 
 *              *req.payload.UserName for dispatcher username at RateLinx
 *              *req.payload.Password for dispatcher password at RateLinx
 *              *req.payload.FromApp for application type
 *              *req.payload.FromDevice for device type
 *              *req.payload.Apps
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.signIn = function (req, reply) {
    //console.log(req.payload);
    async.waterfall(
        [
            function (callback) {
                var options = {
                    url: config.RateLinxAPIURL + '/auth',
                    form: req.payload
                };
                ext_request.post(options, function (err, apires, body) {
                    let res = JSON.parse(body);

                    if (res.Token) {
                        res.dispatcherID = req.payload.clientID.toUpperCase()
                        return callback(null, res);
                    }

                    return callback({ statusNum: 401, statusMsg: res.Message, errFlag: 1 });
                });
            }
        ],
        function (err, sessionDetails) {
            if (err) return reply(err);
            
            var token = jwt.sign(sessionDetails, config.Secret, {
                expiresIn: (moment(sessionDetails.ExpiresOn).unix() - moment().unix()),
                subject: 'dispatcher'
            });

            var accessToken = jwt.sign(sessionDetails, config.Secret, {
                expiresIn: (moment(sessionDetails.ExpiresOn).unix() - moment().unix()),
                subject: 'oauth2'
            });

            reply(status.status(2, { token: token, accessToken: accessToken, expiresIn: (moment(sessionDetails.ExpiresOn).unix() - moment().unix()) }));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: signOut
 * Created On: May 01, 2018
 * Created For: To sign the dispatcher out the application on the basis of successful signin at RateLinx.
 * @Param req:  Object with auth details
 *              *req.auth.credentials.Token for loggedin dispatcher token returned from RateLinx 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.signOut = function (req, reply) {
    async.waterfall(
        [
            function (callback) {
                callback(null, 'success');
            }
        ],
        function (err, res) {
            if (err) return reply(err);

            reply(status.status(2, {}));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: shipmentsFrRL
 * Created On: May 01, 2018
 * Created For: To fetch all the shipments from RateLinx for a logged in dispatcher.
 * @Param req:  Object with auth details
 *              *req.auth.credentials.Token for loggedin dispatcher token returned from RateLinx 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.shipmentsFrRL = (req, reply) => {
    async.waterfall(
        [
            function (callback) {
                var url = config.RateLinxAPIURL + '/shipmentrequests/realtime/';
                ext_request.get(url, {
                    'auth': {
                        'bearer': req.auth.credentials.Token
                    }
                }, function (error, response, body) {
                    if (error) return reply(err);
                    let res = JSON.parse(body);
                    if (res.Message) return callback({ statusNum: 503, statusMsg: res.Message, errFlag: 1 });
                    return callback(null, res);
                });
            }
        ],
        function (err, shipmentList) {
            if (err) return reply(err);

            reply(status.status(2, shipmentList));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: shipmentDetailsFrRL
 * Created On: May 01, 2018
 * Created For: To fetch the details of a shipment from RateLinx for a logged in dispatcher.
 * @Param req:  Object with request data and auth details
 *              *req.payload.driverID for driver ID,
 *              *req.payload.bolNum for bolNum of the shipment
 *              *req.auth.credentials.Token for session token of dispatcher loggedin at RateLinx
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.shipmentDetailsFrRL = (req, reply) => {
    async.waterfall(
        [
            function (callback) {
                var url = config.RateLinxAPIURL + '/shipmentrequests/' + req.params.clientID + '|' + req.params.bolNum; //DEMO|DEMO910915
                ext_request.get(url, {
                    'auth': {
                        'bearer': req.auth.credentials.Token
                    }
                }, function (error, response, body) {
                    if (error) reply(error);
                    let res = JSON.parse(body);
                    if (res.Message) {
                        return callback({ statusNum: 503, statuMsg: res.Message, errFlag: 1 });
                    }
                    return callback(null, res);
                });
            }
        ],
        function (err, shipmentDetails) {
            if (err) return reply(err);

            reply(status.status(2, { 'shipmentDetails': shipmentDetails }));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: shipmentDetails
 * Created On: May 01, 2018
 * Created For: To fetch the details of a shipment from Dispatcher DB for a logged in dispatcher.
 * @Param req:  Object with request data and auth details
 *              *req.payload.driverID for driver ID,
 *              *req.payload.bolNum for bolNum of the shipment
 *              *req.auth.credentials.Token for session token of dispatcher loggedin at RateLinx
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.shipmentDetails = (req, reply) => {
    async.waterfall(
        [
            function (callback) {
                Mongo.SelectOne("shipment_details", { BolNum: req.params.bolNum, ClientID: req.params.clientID }, function (error, shipmentDetails) {
                    if (error) return reply(error);

                    return callback(null, shipmentDetails);
                });                
            }
        ],
        function (err, shipmentDetails) {
            if (err) return reply(err);

            reply(status.status(2, { 'shipmentDetails': shipmentDetails }));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: driversFrRL
 * Created On: May 03, 2018
 * Created For: To fetch the details of a shipment from RateLinx for a logged in dispatcher.
 * @Param req:  Object with request data and auth details
 *              *req.payload.driverID for driver ID,
 *              *req.payload.bolNum for bolNum of the shipment
 *              *req.auth.credentials.dispatcherID for loggedin dispatcher ID 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.driversFrRL = (req, reply) => {
    async.waterfall(
        [
            function (callback) {
                var url = config.RateLinxAPIURL + '/shipmentrequests/drivers';
                ext_request.get(url, {
                    'auth': {
                        'bearer': req.auth.credentials.Token
                    }
                }, function (error, response, body) {
                    if (error) return reply(error);
                    let res = JSON.parse(body);
                    if (res.Message) {
                        return reply({ statusNum: 503, statusMsg: res.Message, errFlag: 1 });
                    }

                    return callback(null, res.Drivers);
                });
                // Mongo.Select("drivers", { ClientID: req.auth.credentials.dispatcherID }, function (err, driverList) {
                //     if (err) {
                //         return callback({ statusNum: 500, statusMsg: 'Database errror', errFlag: 1 });;
                //     }
                //     return callback(null, driverList);
                // });
            }
        ],
        function (err, driverList) {
            if (err) return reply(err);

            reply(status.status(2, { 'driverList': driverList }));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: assignShipmentToDriver
 * Created On: May 04, 2018
 * Created For: To assign a shipment to a driver by a logged in dispatcher.
 * @Param req:  Object with request data and auth details
 *              *req.payload.driverID for driver ID,
 *              *req.payload.bolNum for bolNum of the shipment
 *              *req.auth.credentials.dispatcherID for loggedin dispatcher ID 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.assignShipmentToDriver = (req, reply) => {
    
    async.waterfall(
        [
            function (callback) {
                // first check driver in dispatcher DB
                Mongo.SelectOne("driver_details", { DriverID: req.payload.driverID, DispatcherID: req.auth.credentials.dispatcherID }, function (err, driver) {
                    if (err) {
                        return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
                    }
                    // If driver is not in dispatcher DB then get the details of driver from Ratelinx 
                    if (driver === null) {
    // start driver details from RateLinx
                        var url = config.RateLinxAPIURL + '/shipmentrequests/drivers/' + req.payload.driverID;
                        ext_request.get(url, {
                            'auth': {
                                'bearer': req.auth.credentials.Token
                            }
                        }, function (error, response, body) {
                            if (error) return reply(error);
                            let res = JSON.parse(body);
                            if (res.Message) {
                                return reply({ statusNum: 503, statusMsg: res.Message, errFlag: 1 });
                            }
                            driver = {
                                "_id": null,
                                "DriverID": res.Drivers[0].UserID,
                                "FullName": res.Drivers[0].FullName,
                                "Email": res.Drivers[0].Email,
                                "Mobile": 0,
                                "CountryCode": '',
                                "ImageUrl": '',
                                "DispatcherID": res.Drivers[0].ClientID,
                                "Location":{
                                    "Latitude": 0,
                                    "Longitude": 0
                                }
                            };

                            return callback(null, driver);
                        });
// end driver details from RateLinx
// start driver details from Dispatcher mock API
                        // Mongo.SelectOne("drivers", { UserID: req.payload.driverID }, function (err, driverDetails) {
                        //     if (err) {
                        //         return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
                        //     }
                        //     if (driverDetails === null) {
                        //         return reply({ statusNum: 404, statusMsg: 'driver not found at RateLinx', errFlag: 1 });
                        //     }
                        //     driver = {
                        //         "_id": null,
                        //         "DriverID": driverDetails.UserID,
                        //         "FullName": driverDetails.FullName,
                        //         "Email": driverDetails.Email,
                        //         "Mobile": driverDetails.Mobile,
                        //         "CountryCode": driverDetails.CountryCode,
                        //         "ImageUrl": driverDetails.ImageUrl,
                        //         "DispatcherID": driverDetails.ClientID,
                        //         "Location":{
                        //             "Latitude": 40.74803,
                        //             "Longitude": -73.98488
                        //         }
                        //     };

                        //     return callback(null, driver);
                        // });
// end driver details from Dispatcher mock API
                    } else {
                        return callback(null, driver);
                    }
                });
            },
            function (driver, callback) {
                // check shipment in dispatcher DB
                Mongo.SelectOne("shipment_details", { ClientID: req.payload.clientID, BolNum: req.payload.bolNum, DispatcherID: req.auth.credentials.dispatcherID }, function (err, shipment) {
                    if (err) {
                        return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
                    }
                    var url = config.RateLinxAPIURL + '/shipmentrequests/' + req.payload.clientID + '|' + req.payload.bolNum; //DEMO|DEMO910915
                    ext_request.get(url, {
                        'auth': {
                            'bearer': req.auth.credentials.Token
                        }
                    }, function (error, response, body) {
                        if (error) return reply(error);
                        let res = JSON.parse(body);
                        if (res.Message) {
                            return reply({ statusNum: 503, statusMsg: res.Message, errFlag: 1 });
                        }
                        
                        if (shipment === null) {
                            shipment = {
                                "_id": null,
                                "BolNum": res.BolNum,
                                "ClientID": res.ClientID,
                                "LocID": res.LocID,
                                "DispatcherID": req.auth.credentials.dispatcherID,
                                "AssignedDriverID": "0",
                                "ShipmentDetails": {
                                    "ProNum": res.ProNum,
                                    "Mode": res.Mode,
                                    "PickupStr": res.PickupStr,
                                    "DeliverOnStr": res.DeliverOnStr,
                                    "Status": res.Status,
                                    "DateOpenedStr": res.DateOpenedStr,
                                    "DeadLineStr": res.DeadLineStr,
                                    "OriginAddress": {},
                                    "ShipToAddress": {},
                                    "LineDetails": (res.LineDetails && res.LineDetails.length) ? res.LineDetails[0] : null,
                                    "PickUpLocation": {},
                                    "DropLocation":{},
                                    "DriverReceivedTime": 0,
                                    "DriverOnTheWayTime": 0,
                                    "DriverArrivedTime": 0,
                                    "DriverLoadedStartedTime": 0,
                                    "DriverReachedDropLocationTime": 0,
                                    "DriverDropedTime": 0,
                                    "DriverCompletedTime": 0
                                },
                                "InDispatch": false,
                                "Dispatched": [],
                                "Activities": [],
                                "Status": constants.SHIPMENT_STATUS.NEW.CODE,
                                "StatusText": constants.SHIPMENT_STATUS.NEW.TEXT,
                                "FetchedTS": moment().unix(),                                
                                "CompletedAtTS": 0
                            };                            
                        } else {
                            shipment.ShipmentDetails.LineDetails =  (res.LineDetails && res.LineDetails.length) ? res.LineDetails[0] : {};
                            shipment.ShipmentDetails.PickupStr = res.PickupStr;
                            shipment.ShipmentDetails.DeliverOnStr = res.DeliverOnStr;
                            shipment.ShipmentDetails.DateOpenedStr = res.DateOpenedStr;
                            shipment.ShipmentDetails.DeadLineStr = res.DeadLineStr;
                        }
                        
                        res.Addresses.forEach(address => {
                            if(address.Type != 'BILLTO'){
                            //console.log(address);
                            var key = address.Type.charAt(0) + address.Type.slice(1).toLowerCase();
                            var addressType = key.replace('to', 'To');
                            //console.log(addressType);
                            if(address.Latitude){
                                shipment.ShipmentDetails[addressType + 'Address'] = address;
                            } else {
                                //console.log(shipment.ShipmentDetails[addressType + 'Address']); 
                            var temp = shipment.ShipmentDetails[addressType + 'Address'];  
                            shipment.ShipmentDetails[addressType + 'Address'] = address;                         
                            shipment.ShipmentDetails[addressType + 'Address'].Latitude = temp.Latitude;
                            shipment.ShipmentDetails[addressType + 'Address'].Longitude = temp.Longitude;
                            }
                        }                            
                        }); 
                    
                        return callback(null, driver, shipment);
                    });
                });
            },
            function (driver, shipment, callback) {
                var dispatched = {
                    "DriverID": driver.DriverID,
                    "DriverFullName": driver.FullName,
                    "DriverEmail": driver.Email,
                    "DriverMobile": driver.Mobile,
                    "DriverCountryCode": driver.CountryCode,
                    "DriverPhotoUrl": "",
                    "DriverChn": "driver_" + driver.DriverID,
                    "Status": "Received",
                    "ReceivedAtTS": moment().unix()
                };
                shipment.ShipmentDetails.DriverReceivedTime = moment().unix();
                shipment.InDispatch = true;
                shipment.AssignedDriverID = driver.DriverID;
                shipment.Status = constants.SHIPMENT_STATUS.ASSIGNED.CODE;
                shipment.StatusText = constants.SHIPMENT_STATUS.ASSIGNED.TEXT;
                shipment.Dispatched.push(dispatched);
                shipment.DriverDetails = {
                    'DriverID': driver.DriverID,
                    'FullName': driver.FullName,
                    'Email': driver.Email,
                    'CountryCode': driver.CountryCode || '',
                    'Mobile': driver.Mobile,
                    'PublishChn': driver.PublishChn || '',
                    'AppVersion': driver.AppVersion || '',
                    'BatteryPer': driver.BatteryPer || '',
                    'LocationCheck': driver.LocationCheck | '',
                    'DeviceType': driver.DeviceType || '',
                    'Location': (typeof driver.Location === 'undefined') ? {} : {
                        'Latitude': (typeof driver.Location.Latitude == 'undefined') ? '' : driver.Location.Latitude,
                        'Longitude': (typeof driver.Location.Longitude == 'undefined') ? '' : driver.Location.Longitude
                    }  
                }; 

                if (shipment._id === null) {
                    Mongo.INSERT("shipment_details", shipment, function (err, result) {
                        if (err)
                            return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });                    
                    });
                } else {
                    let queryObj = {
                        query: { BolNum: req.payload.bolNum, ClientID: req.payload.clientID },
                        data: {
                            $set: {
                                'Dispatched': shipment.Dispatched,
                                'InDispatch': shipment.InDispatch,
                                'AssignedDriverID': shipment.AssignedDriverID,
                                'DriverDetails': shipment.DriverDetails,
                                'Status': shipment.Status,
                                'StatusText': shipment.StatusText,
                                'ShipmentDetails.DriverReceivedTime': shipment.ShipmentDetails.DriverReceivedTime,
                                'ShipmentDetails.LineDetails': shipment.ShipmentDetails.LineDetails,
                                'ShipmentDetails.OriginAddress': shipment.ShipmentDetails.OriginAddress,
                                'ShipmentDetails.ShipToAddress': shipment.ShipmentDetails.ShipToAddress,
                                'ShipmentDetails.PickupStr' : shipment.ShipmentDetails.PickupStr,
                                'ShipmentDetails.DeliverOnStr' : shipment.ShipmentDetails.DeliverOnStr,
                                'ShipmentDetails.DateOpenedStr' : shipment.ShipmentDetails.DateOpenedStr,
                                'ShipmentDetails.DeadLineStr': shipment.ShipmentDetails.DeadLineStr
                            }
                        }
                    }
                    Mongo.UPDATE('shipment_details', queryObj, (err, doc) => {                        
                        if (err) {
                            return callback(err);
                        }
                    });                   
                }

                var currentAssignment = {
                    "ClientID": shipment.ClientID,
                    "BolNum": shipment.BolNum,
                    "AssignedAtTS": moment().unix()
                };
                driver.OnJob = true;
                if (driver.currentAssignments) {
                    driver.currentAssignments.push(currentAssignment);
                } else {
                    driver.currentAssignments = [];
                    driver.currentAssignments.push(currentAssignment);
                }
                if (driver._id === null) {
                    Mongo.INSERT("driver_details", driver, function (err, result) {
                        if (err) {
                            return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
                        }
                    });
                } else {
                    let queryObj = {
                        query: { DriverID: driver.DriverID },
                        data: {
                            $set: {
                                'OnJob': driver.OnJob,
                                'currentAssignments': driver.currentAssignments
                            }
                        }
                    }
                    Mongo.UPDATE('driver_details', queryObj, (err, doc) => {
                        if (err) {
                            return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
                        }
                    });
                }

                var options = {
                    url: config.RateLinxAPIURL + '/shipment/' + shipment.ClientID + '|' + shipment.LocID + '|' + shipment.BolNum + '/driverid',
                    auth: {
                        'bearer': req.auth.credentials.Token
                    },
                    form: {
                        'DriverID': driver.DriverID
                        }
                };
                ext_request.post(options, function (err, apiRes, body) {
                    console.log('err', err);                    
                    if (err) {
                        callback({ statusNum: 401, statusMsg: err, errFlag: 1 });
                    }   
                    let res = JSON.parse(body);console.log('res', res);                 
                });

                // start pubnub notification to dispatcher
                let return_ = {
                    status: constants.SHIPMENT_STATUS.SENT.CODE,                    
                    bid: shipment.BolNum, 
                    clientID: shipment.ClientID,                   
                    email: driver.Email,
                    time: moment().unix()
                };
                let dispatcherChannel = 'bookingChn' + req.auth.credentials.dispatcherID;
                notifyi.notifyPunbun({ 'listner': dispatcherChannel, message: return_ });
                // end pubnub notification to dispatcher

                // start redis pub/sub
                client.del('que_' + shipment.ClientID + '_' + shipment.BolNum, function (err, reply) { });
                // end redis pub/sub

                return callback(null, 2);                 
            }
        ],
        function (err, res) {
            if (err) {
                return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
            }

            reply(status.status(2, res));
        }
    );
}

/**
 * Author: Arun Kumar
 * Func Name: allShipments
 * Created On: May 05, 2018
 * Created For: To get all the shipments for a logged in dispatcher from dispatcher DB
 * @Param req:  Object with auth details
 *              *req.auth.credentials.dispatcherID for loggedin dispatcher ID 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.allShipments = (req, reply) => {
    var condition = {DispatcherID: req.auth.credentials.dispatcherID};
    var pageIndex = 0;
    var limit = 2; 
    if (typeof req.payload.startDate !== "undefined" && typeof req.payload.endDate !== "undefined") {
        var startTS = moment(req.payload.startDate).unix();
        var endTS = moment(req.payload.endDate).unix();
        condition = {
            DispatcherID: req.auth.credentials.dispatcherID,
            FetchedTS: {
                '$gte': startTS,
                '$lte': endTS
            }
        }
    }
    if(typeof req.payload.pageIndex !== "undefined"){
        pageIndex = parseInt(req.payload.pageIndex);
    }
    var skip = limit * pageIndex;
    //console.log(condition);
    async.waterfall(
        [
            function (callback) {
                Mongo.SelectWIthLimitSortSkip("shipment_details", condition, { 'FetchedTS': -1 }, limit, skip, (err, shipmentList) => {
                    if (err) {
                        return callback({ statusNum: 500, errFlag: 1, statusMsg: 'Database errror' });;
                    }
                    return callback(null, shipmentList);
                });
            }
        ],
        function (err, shipmentList) {
            if (err) return reply({ statusNum: 503, errFlag: 1, statusMsg: err });

            reply(status.status(2, { 'shipmentList': shipmentList }));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: unAssignedShipments
 * Created On: May 05, 2018
 * Created For: To get un-assigned shipments for a logged in dispatcher from dispatcher DB
 * @Param req:  Object with auth details
 *              *req.auth.credentials.dispatcherID for loggedin dispatcher ID 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.unAssignedShipments = (req, reply) => {
    async.waterfall(
        [
            function (callback) {
                Mongo.Select("shipment_details", { DispatcherID: req.auth.credentials.dispatcherID, Status: constants.SHIPMENT_STATUS.NEW.CODE, InDispatch: false }, function (err, shipmentList) {
                    if (err) {
                        return callback({ statusNum: 500, errFlag: 1, statusMsg: 'Database errror' });;
                    }
                    return callback(null, shipmentList);
                });
            }
        ],
        function (err, shipmentList) {
            if (err) return reply({ statusNum: 503, errFlag: 1, statusMsg: err });

            reply(status.status(2, { 'shipmentList': shipmentList }));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: allDrivers
 * Created On: May 07, 2018
 * Created For: To get all the drivers for a logged in dispatcher from dispatcher DB
 * @Param req:  Object with auth details
 *              *req.auth.credentials.dispatcherID for loggedin dispatcher ID 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.allDrivers = (req, reply) => {
    async.waterfall(
        [
            function (callback) {
                Mongo.Select("driver_details", { DispatcherID: req.auth.credentials.dispatcherID }, function (err, driverList) {
                    if (err) {
                        return callback({ statusNum: 500, errFlag: 1, statusMsg: 'Database errror' });;
                    }
                    return callback(null, driverList);
                });
            }
        ],
        function (err, driverList) {
            if (err) reply({ statusNum: 503, errFlag: 1, statusMsg: err });

            reply(status.status(2, { 'driverList': driverList }));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: driverDetails
 * Created On: May 07, 2018
 * Created For: To get the details of a driver for a logged in dispatcher from dispatcher DB
 * @Param req:  Object with auth details, url parameters
 *              *req.auth.credentials.dispatcherID for loggedin dispatcher ID 
 *              *req.params.driverID for driver ID
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.driverDetails = (req, reply) => {
    async.waterfall(
        [
            function (callback) {
                Mongo.SelectOne("driver_details", { DriverID: req.params.driverID, DispatcherID: req.auth.credentials.dispatcherID }, function (err, driver) {
                    if (err) {
                        return callback({ statusNum: 500, errFlag: 1, statusMsg: 'Database errror' });;
                    }
                    if (driver === null) {
                        return callback({ statusNum: 404, errFlag: 1, statusMsg: 'driver not found' });;
                    }
                    return callback(null, driver);
                });
            }
        ],
        function (err, driver) {
            if (err) return reply({ statusNum: 503, errFlag: 1, statusMsg: err });

            reply(status.status(2, { 'driverDetails': driver }));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: confirmShipmentFrRL
 * Created On: May 10, 2018
 * Created For: To confirm an awarded shipment using API at RateLinx by logged in dispatcher.
 * @Param req:  Object with auth details and payload data
 *              *req.auth.credentials.Token for loggedin dispatcher token returned from RateLinx 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.confirmOrDenyShipmentFrRL = (req, reply) => {
    //console.log(req.payload);
    async.waterfall(
        [
            function (callback) {
                var url = config.RateLinxAPIURL + '/shipmentrequests/' + req.payload.clientID + '|' + req.payload.bolNum; //DEMO|DEMO910915
                ext_request.get(url, {
                    'auth': {
                        'bearer': req.auth.credentials.Token
                    }
                }, function (error, apiRes, body) {
                    if (error) return reply(error);
                    let res = JSON.parse(body);
                    if (res.Message) {
                        return reply({ statusNum: 503, statusMsg: res.Message, errFlag: 1 });
                    }
                    callback(null, res);
                });
            },
            function (shipmentDetails, callback) {
                var bidID = shipmentDetails.Entries.length ? shipmentDetails.Entries[0].ID : 0;
                if (req.payload.action == 1) {
                    var data = { ProNumber: req.payload.proNumber };
                    var action = 'confirm';
                } else {
                    var data = { DenyReason: req.payload.denyReason };
                    var action = 'deny';
                }
                var options = {
                    url: config.RateLinxAPIURL + '/shipmentrequests/' + shipmentDetails.ClientID + '|' + shipmentDetails.LocID + '|' + shipmentDetails.BolNum + '/' + bidID + '/' + action,
                    auth: {
                        'bearer': req.auth.credentials.Token
                    },
                    form: data
                };
                ext_request.post(options, function (err, apiRes, body) {
                    let res = JSON.parse(body);
                    //console.log(res);
                    if (res == 'Success') {
                        return callback(null, res);
                    }

                    callback({ statusNum: 401, statusMsg: res.Message, errFlag: 1 });
                });
            }
        ],
        function (err, res) {
            if (err) return reply(err);

            reply(status.status(2, res));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: insertRLShipments
 * Created On: May 31, 2018
 * Created For: To insert all the new shipments from RateLinx into dispatcher DB.
 * @Param req:  Object with auth details
 *              *req.auth.credentials.Token for loggedin dispatcher token returned from RateLinx 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.insertRLShipments = (req, reply) => {
    
    async.waterfall(
        [
            function (callback) {
                var url = config.RateLinxAPIURL + '/shipmentrequests/realtime/';
                ext_request.get(url, {
                    'auth': {
                        'bearer': req.auth.credentials.Token
                    }
                }, function (error, response, body) {
                    if (error) return reply(error);

                    let res = JSON.parse(body); 
                    if (res.Message) {
                        res = { FuturePickups: [] };
                    }

                    return callback(null, res);
                });
            },
            function (shipmentsRL, callback) {
                Mongo.SELECT("shipment_details", { p: { ClientID: 1, BolNum: 1, _id: 0 } }, function (error, shipmentsD) {
                    if (error) return reply(error);
                    var shipmentsDR = [];
                    shipmentsD.forEach(shipment => {
                        shipmentsDR.push(shipment.ClientID+shipment.BolNum);
                    });

                    return callback(null, shipmentsRL, shipmentsDR);
                });
            },
            function (shipmentsRL, shipmentsDR, callback) {
                var shipmentsArr = [];
                shipmentsRL.FuturePickups.forEach(shipment => {
                    if (shipmentsDR.indexOf(shipment.ClientID+shipment.BolNum) === -1 && shipment.DriverID == "" ) { 
                        var shipmentObj = {
                            "BolNum": shipment.BolNum,
                            "ClientID": shipment.ClientID,
                            "LocID": shipment.LocID,
                            "DispatcherID": req.auth.credentials.dispatcherID,
                            "AssignedDriverID": "0",
                            "ShipmentDetails": {
                                "ProNum": shipment.ProNum,
                                "Mode": shipment.Mode,
                                "PickupStr": shipment.PickupStr,
                                "DeliverOnStr": shipment.DeliverOnStr,
                                "Status": shipment.Status,
                                "DateOpenedStr": "",
                                "DeadLineStr": "",
                                "OriginAddress": {
                                    "Company": shipment.OriginCompany,
                                    "Address1": shipment.OriginAddress1,
                                    "Address2": shipment.OriginAddress2,
                                    "Address3": shipment.OriginAddress3,
                                    "Address4": "",
                                    "City": shipment.OriginCity,
                                    "State": shipment.OriginState,
                                    "Zip": shipment.OriginZip,
                                    "Country": "",
                                    "Phone": shipment.OriginPhone,
                                    "Email": shipment.OriginEmail,
                                    "Fax": "",
                                    "Latitude": (shipment.OriginLatitude)?shipment.OriginLatitude:parseFloat(0), //28.621175
                                    "Longitude": (shipment.OriginLongitude)?shipment.OriginLongitude:parseFloat(0) //77.378779 
                                },
                                "ShipToAddress": {
                                    "City": shipment.ShipToCity,
                                    "State": shipment.ShipToState,
                                    "Zip": shipment.ShipToZip,
                                    "Latitude": (shipment.ShipToLatitude)?shipment.ShipToLatitude: parseFloat(0), //28.608301
                                    "Longitude": (shipment.ShipToLongitude)?shipment.ShipToLongitude:parseFloat(0) //77.372536
                                },
                                "LineDetails": (shipment.LineDetails && res.LineDetails.length) ? shipment.LineDetails[0] : null,
                                "PickUpLocation": {
                                    "Latitude": (shipment.OriginLatitude)?shipment.OriginLatitude: parseFloat(0), //28.621175
                                    "Longitude": (shipment.OriginLongitude)?shipment.OriginLongitude: parseFloat(0) //77.378779
                                },
                                "DriverReceivedTime": 0,
                                "DriverOnTheWayTime": 0,
                                "DriverArrivedTime": 0,
                                "DriverLoadedStartedTime": 0,
                                "DriverReachedDropLocationTime": 0,
                                "DriverDropedTime": 0,
                                "DriverCompletedTime": 0
                            },
                            "InDispatch": false,
                            "Dispatched": [],
                            "Activities": [],
                            "Status": constants.SHIPMENT_STATUS.NEW.CODE,
                            "StatusText": constants.SHIPMENT_STATUS.NEW.TEXT,
                            "FetchedTS": moment().unix(), 
                            "CompletedAtTS": 0
                        };

                        shipmentsArr.push(shipmentObj);
                    }
                });
                
                if (shipmentsArr.length) {
                    Mongo.InsertMany("shipment_details", shipmentsArr, function (error, response) {
                        if (error) return reply(error);
                        shipmentsArr.forEach(shipment => {
                            var latitude = (typeof shipment.PickUpLocation == "undefined" ? "" : shipment.PickUpLocation.Latitude);
                            var longitude = (typeof shipment.PickUpLocation == "undefined" ? "" : shipment.PickUpLocation.Longitude);

                            client.hmset(
                                'que_' + shipment.ClientID + '_' + shipment.BolNum,
                                'status', constants.SHIPMENT_STATUS.NEW.CODE,
                                'latitude', latitude,
                                'longitude', longitude,
                            );
                            let expireTime = parseDateString(shipment.ShipmentDetails.PickupStr)-moment().utc().unix();
                            if(!expireTime) expireTime = 3600;
                            console.log(expireTime);
                            client.expire('que_' + shipment.ClientID + '_' + shipment.BolNum, expireTime);  
                            // client.hgetall('que_' + shipment.ClientID + '_' + shipment.BolNum, function (err, reply) {
                            //     console.log(err);
                            //     console.log(reply); 
                            // });
                            let return_ = {
                                status: constants.SHIPMENT_STATUS.NEW.CODE,
                                bid: shipment.BolNum,
                                clientID: shipment.ClientID,
                                time: moment().unix(),
                                email: 'na'
                            };
                            let dispatcherChannel = 'bookingChn' + req.auth.credentials.dispatcherID;
                            notifyi.notifyPunbun({ 'listner': dispatcherChannel, message: return_ });                       
                        });                        

                        return callback(null, response);
                    });
                } else {
                    response = "No data to insert";

                    return callback(null, response);
                }
            }
        ],
        function (err, res) {
            if (err) return reply(err);

            reply(status.status(2, res));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: assignShipmentToDriverMobile
 * Created On: Jun 04, 2018
 * Created For: To assign a shipment to a driver by a logged in dispatcher.
 * @Param req:  Object with request data and auth details
 *              *req.payload.driverID for driver ID,
 *              *req.payload.bolNum for bolNum of the shipment
 *              *req.payload.dispatcherID for clientID of dispatcher at RateLinx 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.assignShipmentToDriverMobile = (req, reply) => {
    
    async.waterfall(
        [
            function (callback) {
                // first check driver in dispatcher DB
                Mongo.SelectOne("driver_details", { DriverID: req.payload.driverID, DispatcherID: req.payload.dispatcherID }, function (err, driver) {
                    if (err) {
                        return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
                    }
                    // If driver is not in dispatcher DB then get the details of driver from Ratelinx 
                    if (driver === null) {
                        var url = config.RateLinxAPIURL + '/shipmentrequests/drivers/' + req.payload.driverID;
                        ext_request.get(url, {
                            'auth': {
                                'bearer': req.payload.dispatcherToken
                            }
                        }, function (error, response, body) {
                            if (error) return reply(error);
                            let res = JSON.parse(body);
                            if (res.Message) {
                                return reply({ statusNum: 503, statusMsg: res.Message, errFlag: 1 });
                            }
                            driver = {
                                "_id": null,
                                "DriverID": res.Drivers[0].UserID,
                                "FullName": res.Drivers[0].FullName,
                                "Email": res.Drivers[0].Email,
                                "Mobile": 0,
                                "CountryCode": '',
                                "ImageUrl": '',
                                "DispatcherID": res.Drivers[0].ClientID,
                                "Location":{
                                    "Latitude": 0,
                                    "Longitude": 0
                                }
                            };

                            return callback(null, driver);
                        });
                        // Mongo.SelectOne("drivers", { UserID: req.payload.driverID }, function (err, driverDetails) {
                        //     if (err) {
                        //         return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
                        //     }
                        //     if (driverDetails === null) {
                        //         return reply({ statusNum: 404, statusMsg: 'driver not found at RateLinx', errFlag: 1 });
                        //     }
                        //     driver = {
                        //         "_id": null,
                        //         "DriverID": driverDetails.UserID,
                        //         "FullName": driverDetails.FullName,
                        //         "Email": driverDetails.Email,
                        //         "Mobile": driverDetails.Mobile,
                        //         "CountryCode": driverDetails.CountryCode,
                        //         "ImageUrl": driverDetails.ImageUrl,
                        //         "DispatcherID": req.payload.dispatcherID,
                        //         "Location":{
                        //             "Latitude": 40.74803,
                        //             "Longitude": -73.98488
                        //         }
                        //     };

                        //     return callback(null, driver);
                        // });
                    } else {

                        return callback(null, driver);
                    }
                });
            },
            function (driver, callback) {
                // check shipment in dispatcher DB
                Mongo.SelectOne("shipment_details", { ClientID: req.payload.clientID, BolNum: req.payload.bolNum, DispatcherID: req.payload.dispatcherID }, function (err, shipment) {
                    if (err) {
                        return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
                    }
                    var url = config.RateLinxAPIURL + '/shipmentrequests/' + req.payload.clientID + '|' + req.payload.bolNum; //DEMO|DEMO910915
                    ext_request.get(url, {
                        'auth': {
                            'bearer': req.payload.dispatcherToken
                        }
                    }, function (error, response, body) {
                        if (error) return reply(error);
                        
                        let res = JSON.parse(body);
                        if (res.Message) {
                            return reply({ statusNum: 503, statusMsg: res.Message, errFlag: 1 });
                        }
                        
                        if (shipment == null) {
                            shipment = {
                                "_id": null,
                                "BolNum": res.BolNum,
                                "ClientID": res.ClientID,
                                "LocID": res.LocID,
                                "DispatcherID": req.payload.dispatcherID,
                                "AssignedDriverID": "0",
                                "ShipmentDetails": {
                                    "ProNum": res.ProNum,
                                    "Mode": res.Mode,
                                    "PickupStr": res.PickupStr,
                                    "DeliverOnStr": res.DeliverOnStr,
                                    "Status": res.Status,
                                    "DateOpenedStr": res.DateOpenedStr,
                                    "DeadLineStr": res.DeadLineStr,
                                    "OriginAddress": {},
                                    "ShipToAddress": {},
                                    "LineDetails": (res.LineDetails && res.LineDetails.length) ? res.LineDetails[0] : {},
                                    "PickUpLocation": {},
                                    "DropLocation":{},
                                    "DriverReceivedTime": 0,
                                    "DriverOnTheWayTime": 0,
                                    "DriverArrivedTime": 0,
                                    "DriverLoadedStartedTime": 0,
                                    "DriverReachedDropLocationTime": 0,
                                    "DriverDropedTime": 0,
                                    "DriverCompletedTime": 0
                                },
                                "InDispatch": false,
                                "Dispatched": [],
                                "Activities": [],
                                "Status": constants.SHIPMENT_STATUS.NEW.CODE,
                                "StatusText": constants.SHIPMENT_STATUS.NEW.TEXT,
                                "FetchedTS": moment().unix(),                                
                                "CompletedAtTS": 0
                            };                            
                        } else {
                            shipment.ShipmentDetails.LineDetails =  (res.LineDetails && res.LineDetails.length) ? res.LineDetails[0] : {};
                            shipment.ShipmentDetails.PickupStr = res.PickupStr;
                            shipment.ShipmentDetails.DeliverOnStr = res.DeliverOnStr;
                            shipment.ShipmentDetails.DateOpenedStr = res.DateOpenedStr;
                            shipment.ShipmentDetails.DeadLineStr = res.DeadLineStr;
                        }
                        
                        res.Addresses.forEach(address => {
                            if(address.Type != 'BILLTO'){
                            //console.log(address);
                            var key = address.Type.charAt(0) + address.Type.slice(1).toLowerCase();
                            var addressType = key.replace('to', 'To');
                            //console.log(addressType);
                            if(address.Latitude){
                                shipment.ShipmentDetails[addressType + 'Address'] = address;
                            } else {
                                //console.log(shipment.ShipmentDetails[addressType + 'Address']); 
                            var temp = shipment.ShipmentDetails[addressType + 'Address'];  
                            shipment.ShipmentDetails[addressType + 'Address'] = address;                         
                            shipment.ShipmentDetails[addressType + 'Address'].Latitude= temp.Latitude;
                            shipment.ShipmentDetails[addressType + 'Address'].Longitude= temp.Longitude;
                            }
                        }                            
                        });
                    
                        return callback(null, driver, shipment);
                    });
                });
            },
            function (driver, shipment, callback) {
                var dispatched = {
                    "DriverID": driver.DriverID,
                    "DriverFullName": driver.FullName,
                    "DriverEmail": driver.Email,
                    "DriverMobile": driver.Mobile,
                    "DriverCountryCode": driver.CountryCode,
                    "DriverPhotoUrl": "",
                    "DriverChn": "driver_" + driver.DriverID,
                    "Status": "Received",
                    "ReceivedAtTS": moment().unix()
                };
                shipment.ShipmentDetails.DriverReceivedTime = moment().unix();
                shipment.InDispatch = true;
                shipment.AssignedDriverID = driver.DriverID;
                shipment.Status = constants.SHIPMENT_STATUS.ASSIGNED.CODE;
                shipment.StatusText = constants.SHIPMENT_STATUS.ASSIGNED.TEXT;
                shipment.Dispatched.push(dispatched);
                shipment.DriverDetails = {
                    'DriverID': driver.DriverID,
                    'FullName': driver.FullName,
                    'Email': driver.Email,
                    'CountryCode': driver.CountryCode || '',
                    'Mobile': driver.Mobile,
                    'PublishChn': driver.PublishChn || '',
                    'AppVersion': driver.AppVersion || '',
                    'BatteryPer': driver.BatteryPer || '',
                    'LocationCheck': driver.LocationCheck | '',
                    'DeviceType': driver.DeviceType || '',
                    'Location': (typeof driver.Location === 'undefined') ? {} : {
                        'Latitude': (typeof driver.Location.Latitude == 'undefined') ? '' : driver.Location.Latitude,
                        'Longitude': (typeof driver.Location.Longitude == 'undefined') ? '' : driver.Location.Longitude
                    }  
                }; 

                if (shipment._id === null) {
                    Mongo.INSERT("shipment_details", shipment, function (err, result) {
                        if (err)
                            return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });                    
                    });
                } else {
                    let queryObj = {
                        query: { BolNum: req.payload.bolNum, ClientID: req.payload.clientID },
                        data: {
                            $set: {
                                'Dispatched': shipment.Dispatched,
                                'InDispatch': shipment.InDispatch,
                                'AssignedDriverID': shipment.AssignedDriverID,
                                'DriverDetails': shipment.DriverDetails,
                                'Status': shipment.Status,
                                'StatusText': shipment.StatusText,
                                'ShipmentDetails.DriverReceivedTime': shipment.ShipmentDetails.DriverReceivedTime,
                                'ShipmentDetails.LineDetails': shipment.ShipmentDetails.LineDetails,
                                'ShipmentDetails.OriginAddress': shipment.ShipmentDetails.OriginAddress,
                                'ShipmentDetails.ShipToAddress': shipment.ShipmentDetails.ShipToAddress,
                                'ShipmentDetails.PickupStr' : shipment.ShipmentDetails.PickupStr,
                                'ShipmentDetails.DeliverOnStr' : shipment.ShipmentDetails.DeliverOnStr,
                                'ShipmentDetails.DateOpenedStr' : shipment.ShipmentDetails.DateOpenedStr,
                                'ShipmentDetails.DeadLineStr': shipment.ShipmentDetails.DeadLineStr
                            }
                        }
                    }
                    Mongo.UPDATE('shipment_details', queryObj, (err, doc) => {
                        
                        if (err) {
                            return callback(err);
                        }
                    });                   
                }

                var currentAssignment = {
                    "BolNum": shipment.BolNum,
                    "ClientID": shipment.ClientID,
                    "AssignedAtTS": moment().unix()
                };
                driver.OnJob = true;
                if (driver.currentAssignments) {
                    driver.currentAssignments.push(currentAssignment);
                } else {
                    driver.currentAssignments = [];
                    driver.currentAssignments.push(currentAssignment);
                }
                if (driver._id === null) {
                    Mongo.INSERT("driver_details", driver, function (err, result) {
                        if (err) {
                            return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
                        }
                    });
                } else {
                    let queryObj = {
                        query: { DriverID: driver.DriverID },
                        data: {
                            $set: {
                                'OnJob': driver.OnJob,
                                'currentAssignments': driver.currentAssignments
                            }
                        }
                    }
                    Mongo.UPDATE('driver_details', queryObj, (err, doc) => {
                        if (err) {
                            return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
                        }
                    });
                }

                var options = {
                    url: config.RateLinxAPIURL + '/shipment/' + shipment.ClientID + '|' + shipment.LocID + '|' + shipment.BolNum + '/driverid',
                    auth: {
                        'bearer': req.payload.dispatcherToken
                    },
                    form: {
                        'DriverID': driver.DriverID
                        }
                };
                ext_request.post(options, function (err, apiRes, body) {
                    console.log('err', err);                    
                    if (err) {
                        callback({ statusNum: 401, statusMsg: err, errFlag: 1 });
                    }   
                    let res = JSON.parse(body);console.log('res', res);                 
                });

                // start pubnub notification to dispatcher
                let return_ = {
                    status: constants.SHIPMENT_STATUS.SENT.CODE,
                    bid: shipment.BolNum,
                    clientID: shipment.ClientID,
                    email: driver.Email,
                    time: moment().unix()
                };
                let dispatcherChannel = 'bookingChn' + req.payload.dispatcherID;
                notifyi.notifyPunbun({ 'listner': dispatcherChannel, message: return_ });
                // end pubnub notification to dispatcher

                // start redis pub/sub
                client.del('que_' + shipment.ClientID + '_' + shipment.BolNum, function (err, reply) { });
                // end redis pub/sub

                return callback(null, 2);    
            }
        ],
        function (err, res) {
            if (err) {
                return reply({ statusNum: 500, statusMsg: err, errFlag: 1 });
            }

            reply(status.status(2, res));
        }
    );
}

/**
 * Author: Arun Kumar
 * Func Name: allDriversMobile
 * Created On: Jun 04, 2018
 * Created For: To get all the drivers for a logged in dispatcher from dispatcher DB
 * @Param req:  Object with auth details
 *              *req.payload.dispatcherID for clientID of dispatcher at RateLinx 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.allDriversMobile = (req, reply) => {
    async.waterfall(
        [
            function (callback) {
                Mongo.Select("driver_details", { DispatcherID: req.params.dispatcherID.toUpperCase() }, function (err, driverList) {
                    if (err) {
                        return callback({ statusNum: 500, errFlag: 1, statusMsg: 'Database errror' });;
                    }
                    return callback(null, driverList);
                });
            }
        ],
        function (err, driverList) {
            if (err) reply({ statusNum: 503, statusMsg: err, errFlag: 1, });

            reply(status.status(2, { 'driverList': driverList }));
        });
}

/**
 * Author: Arun Kumar
 * Func Name: activityPath
 * Created On: Jun 10, 2018
 * Created For: To get the activity path of a shipment from dispatcher DB
 * @Param req:  Object with auth details
 *              *req.params.bolNum for bolNum of shipment at RateLinx 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.activityPath = (req, reply) => {

    Mongo.SelectOne('location_logs', { ClientID: req.params.clientID, BolNum: req.params.bolNum }, (err, doc) => {
        if (err)
            return reply(status.status(3));

        if (doc === null)
            return reply(status.status(9))

        return reply(status.status(2, doc));
    });
}

/**
 * Author: Arun Kumar
 * Func Name: getAppConfig
 * Created On: Jun 10, 2018
 * Created For: To get the app configuration for pubnub.
 * @Param request:  Object with auth details
 *              *req.auth.credentials.Token for loggedin dispatcher token returned from RateLinx 
 * @Param reply:  function to respond to the request
 * @return: return data in json format
 **/
module.exports.getAppConfig = function (request, reply) {
    reply(status.status(2, {
        pubnubkeys: {
            "publishKey": config.PUBNUB_PUBLISH_KEY,
            "subscribeKey": config.PUBNUB_SUBSCRIBE_KEY
        }
    }
    ));
}

/**
 * Author: Arun Kumar
 * Func Name: parseDateString
 * Created On: June 9, 2018
 * Created For: To parse the date string into unix timestamp.
 * @Param dateStr: date string 
 * @return: return unix timestamp
 **/
function parseDateString(dateStr){
    //eg- dateStr = '7/4/2018 8:00 AM - 5:00 PM CDT';
    var dateArr = dateStr.split('-');
    if (dateArr[1]) {
        var timeArr = dateArr[1].trim().split(' ');
        if (timeArr[2]) {
            var tz = timeArr[2]; 
            if(tz.trim() == 'CDT'){
               tzOffset = '-05:00';
            } else tzOffset = '+00:00';

           return moment.parseZone(dateArr[0]+tzOffset, 'M/D/YYYY h:mm A Z').unix(); //unix timestamp            
        }
    }
    
}





